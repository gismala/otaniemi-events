var classes = ["1", "2", "3", "4", "5", "6", "7"];
var sqlClause = "SELECT * FROM tapahtumat WHERE luokka IN "+"("+classes.join()+")";
var mintime = null;
var maxtime = null;
var imgbase = './static/Images/';
var saunaImg = imgbase+'sauna2.png';
var sportImg = imgbase + 'sports2.png';
var eduImg =imgbase + 'edu3.png'; 
var partyImg = imgbase + 'party2.png';
var thesImg = imgbase + 'thesis4.png';
var artImg =  imgbase + 'arts3.png';
var miscImg = imgbase + 'misc2.png';
var clustImg = imgbase + 'klusteri3.png';
var heatmapBase = './static/Heatmaps';

(function (a) { (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)) })(navigator.userAgent || navigator.vendor || window.opera);
var isMobile = jQuery.browser.mobile;

window.onload = function () {
    try {
        // Create a map object with default center point and zoom level
        map = new L.Map('map', {
            zoomControl: false,
            center: [60.184284718, 24.8283175196],
            zoom: 15,
            minZoom: 14
        });

        // Title
        var title = L.control();
        title.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'ctl title');
            this.update();
            return this._div;
        };
        title.update = function (props) {
            this._div.innerHTML = "What's happening in Otaniemi?!";
        };
        //title.addTo(map);
        
        var layerControl = undefined;
        var kokeilu = undefined;
        
        setIcons();
        setLegend();
        loadData();
        loadMainData();        
        
        var locButton = L.control({ position: 'bottomleft' });

        locButton.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'location');
            div.innerHTML += '<img id="getposition" src="' + clustImg + '">';
            return div;
        };
        locButton.addTo(map);
        $('#getposition').on('click', function () {
            map.locate({ setView: true, maxZoom: 15 });
        });
        L.control.zoom({
            position: 'bottomright'
        }).addTo(map);
        
        // Add Scale Bar to Map
        L.control.scale({ position: 'bottomleft', imperial: 'False'}).addTo(map);
    } catch (e) {
        console.log(e);
    }
}

function setIcons() {
    //Set Icons
    var LeafIcon = L.Icon.extend({
        options: {
            iconSize: [30, 30],
            iconAnchor: [15, 15],
            popupAnchor: [1, -10],
        }
    });

    saunaicon = new LeafIcon({ iconUrl:    saunaImg});
    sporticon = new LeafIcon({ iconUrl:    sportImg});
    eduicon = new LeafIcon({ iconUrl:      eduImg});
    partyicon = new LeafIcon({ iconUrl:    partyImg});
    thesisicon = new LeafIcon({ iconUrl:   thesImg });
    articon = new LeafIcon({ iconUrl:      artImg });
    miscicon = new LeafIcon({ iconUrl:     miscImg });
    clusterIcon = new LeafIcon({ iconUrl: clustImg });
    addMarker = function () {
        return L.markerClusterGroup({
            spiderfyOnMaxZoom: false, showCoverageOnHover: false, zoomToBoundsOnClick: false, maxClusterRadius: 20, spiderfyDistanceMultiplier: 2            ,
            iconCreateFunction: function (cluster) {
                return clusterIcon;
            },
        });
    }
    markers = addMarker();
}

function loadData() {
    var labels = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png', {});
    labels.addTo(map);
    L.tileLayer("http://www.htmlcsscolor.com/preview/128x128/DADADA.png").addTo(map);
    
    var ca1 = 'https://joolaine.carto.com/api/v2/viz/2b1f2676-b24c-11e6-a131-0ee66e2c9693/viz.json';
    cartodb.createLayer(map, ca1).addTo(map);
    var opa = 0.90;
    var education = L.tileLayer(heatmapBase + '/education/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var sauna = L.tileLayer(heatmapBase + '/sauna/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var art = L.tileLayer(heatmapBase + '/art/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var thesis = L.tileLayer(heatmapBase + '/thesis/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var party = L.tileLayer(heatmapBase + '/party/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var sport = L.tileLayer(heatmapBase + '/sport/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var other = L.tileLayer(heatmapBase + '/other/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var syyskuu = L.tileLayer(heatmapBase + '/syyskuu/{z}/{x}/{y}.png', { tms: true, opacity: opa, attribution: "" });
    var basemaps = {};
    var overlaymaps = {
        "Street names": labels,
        "Sauna heatmap": sauna,
        "Sport heatmap": sport,
        "Educational heatmap": education,
        "Party heatmap": party,
        "Thesis heatmap": thesis,
        "Art heatmap": art,
        "Misc heatmap": other,
        "September heatmap": syyskuu
    };

    // Add base layers
    layerControl = L.control.layers({}, overlaymaps, { collapsed: true , position:"topleft"}).addTo(map);
}
function loadMainData() {
    var i = 0;
    try {
        $.getJSON("https://joolaine.carto.com/api/v2/sql?format=GeoJSON&q=" + sqlClause, function (data) {
            kokeilu = L.geoJson(data, {
                onEachFeature: function (feature, layer) {
                    layer.bindPopup('<b>Event</b><br>' + feature.properties.tapahtuma +
                                    '<br><b>Date</b><br>' + feature.properties.paivama.substr(0, 10) +
                                    '<br><b>Location</b><br>' + feature.properties.paikka + '');
                    layer.cartodb_id = feature.properties.cartodb_id;
                },
                pointToLayer: function (feature, latlng) {
                    try {
                        i++;
                        switch (feature.properties.luokka) {
                            case 1: var marker = L.marker(latlng, { icon: sporticon }); break;
                            case 2: var marker = L.marker(latlng, { icon: saunaicon }); break;
                            case 3: var marker = L.marker(latlng, { icon: thesisicon }); break;
                            case 4: var marker = L.marker(latlng, { icon: eduicon }); break;
                            case 5: var marker = L.marker(latlng, { icon: partyicon }); break;
                            case 6: var marker = L.marker(latlng, { icon: articon }); break;
                            case 7: var marker = L.marker(latlng, { icon: miscicon }); break;
                            default: var marker = L.marker(latlng, { icon: saunaicon }); break;
                        }
                        markers.addLayer(marker);
                        return marker;
                    } catch (e) {
                        console.log(e);
                    }
                }
            })
            createSlider();

        });
        if (mintime === null && classes.length === 7) {
            markers.on('clusterclick', function (a) {
                a.layer.spiderfy();
            });
            map.addLayer(markers);
            layerControl.addOverlay(markers, "Events");

        }
        console.log("Map layers rendered without problems");
    } catch (e) {
        console.log("There were problems with map rendering:");
        console.log(e);
    }
}


function setLegend()
{
    try {
        var Legend = new L.Control.Legend({
            position: 'topright',
            collapsed: isMobile,
            controlButton: {
                title: "Legend"
            }
        });
        map.addControl(Legend);

        $(".legend-container").append($("#legend"));
        //$(".legend-toggle").append( "<i class='legend-toggle-icon' style='color: #000'>lolleroo</i>" );
        $(".legend-toggle").append("<p class='legend-toggle-icon' style='color: #000'>i</>");
        $("#legend").append('<p><img id="2" src="' + saunaImg + '">Sauna</p>');
        $("#legend").append('<p><img id="1"src="' + sportImg + '">Sport</p>');
        $("#legend").append('<p><img id="4" src="' + eduImg + '">Education</p>');
        $("#legend").append('<p><img id="5" src="' + partyImg + '">Party</p>');
        $("#legend").append('<p><img id="3" src="' + thesImg + '">Thesis</p>');
        $("#legend").append('<p><img id="6" src="' + artImg + '">Art</p>');
        $("#legend").append('<p><img id="7" src="' + miscImg + '">Misc</p>');
        $("#legend").append('<p><img id="999" src="' + clustImg + '">Multiple</p>');

        $("#legend img").on("click", function () {
            if (!$(this).hasClass("clicked")) {
                $(this).addClass("clicked");
                filterClasses(this.id);
            } else {
                $(this).removeClass("clicked");
                restoreClasses(this.id);
            }
        });

    } catch (e) {
        console.log(e);
    }
    
}

function setLegendOrig() {
    // Create Leaflet Control Object for Legend
    var legend = L.control({ position: 'topright' });

    function showDisclaimer() {
        var div = document.getElementById("info legend")
        div.innerHTML = "<h6>DISCLAIMER:<br>text</h3><table></table>";
    }

    function hideDisclaimer() {
        var div = document.getElementById("info legend")
        div.innerHTML = "<h2>DISCLAIMER</h2>";
    }
    // Function that runs when legend is added to map
    legend.onAdd = function (map) {
        // Create Div Element and Populate it with HTML
        var div = L.DomUtil.create('div', 'legend');
        div.innerHTML += '<b>Events in Otaniemi</b><br />';
        div.innerHTML += '<br/><br/><br/><br/><img id="sauna" src="' + saunaImg + '">Sauna';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + sportImg + '">Sport';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + eduImg + '">Education';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + partyImg + '">Party';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + thesImg + '">Thesis';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + artImg + '">Art';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + miscImg + '">Misc.';
        div.innerHTML += '<br/><br/><br/><br/><img src="' + clustImg + '">Multiple';
        div.setAttribute("onmouseenter", "showDisclaimer()");
        div.setAttribute("onmouseleave", "hideDisclaimer()");


        // Return the Legend div containing the HTML content
        return div;
    };

    // Add Legend to Map
    map.addControl(legend);
    function notify() {

        
    }
    $("#sauna").on("click", function () {
        if (!$(this).hasClass("clicked"))
        {
            map.removeControl(legend);
            $(this).addClass("clicked");
            map.addControl(legend);
            
        } else {
            map.removeControl(legend);
            $(this).removeClass("clicked");
            map.addControl(legend);
        }
    });

}

function createSlider() {
    if (mintime === null && classes.length === 7 && !isMobile) {
        var sliderControl = L.control.sliderControl({ position: "topright", layer: kokeilu, timeAttribute: "paivama", range: true, alwaysShowDate: true });
        map.addControl(sliderControl);
        sliderControl.startSlider();
        $('#slider-timestamp').html("Select time range");
    }
}
function setSQL(min, max) {
    mintime = min;
    maxtime = max;
}

function filterClasses(id) {
    var i = classes.indexOf(id);
    if (i > -1) {
        classes.splice(i, 1);
        refreshEvents();
    }


}

function restoreClasses(id) {
    var i = classes.indexOf(id);
    if (i === -1) {
        classes.push(id);
        refreshEvents();
    };
}

function refreshEvents() {
    sqlClause = "SELECT * FROM tapahtumat WHERE luokka IN " + "(" + classes.join() + ")";
    if (mintime != null && maxtime != null) {
        sqlClause += " AND paivama >= '" + mintime + "'";
        sqlClause += " AND paivama <= '" + maxtime + "'";
    }
    markers.clearLayers();
    loadMainData();
    
}

window.setInterval(function () {
    if (mintime != null && maxtime != null) {
        var sql2 = "SELECT * FROM tapahtumat WHERE luokka IN "+"("+classes.join()+")";
        sql2 += " AND paivama >= '" + mintime + "'";
        sql2 += " AND paivama <= '" + maxtime + "'";
        if (sql2.localeCompare(sqlClause) != 0) {
            sqlClause = sql2;
            markers.clearLayers();
            loadMainData();
        }

    }
}, 1000);




