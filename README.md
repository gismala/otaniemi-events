# Events in Otaniemi Map

[![Build status](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://gitlab.com/gismala/otaniemi-events/commits/master)



Demo site: https://gismala.gitlab.io/otaniemi-events/

Events in Otaniemi Map is an interactive demo map showing different kinds of events in Otaniemi area. 

This was a demo for a university cartography project course. Our team won the competition of the best project.
This was also very first web programming project.

### Technologies used
* Carto and Cartodb JS library
* Leaflet and plugins: Markercluster, LeafletSlider and Leaflet-Legend
* JQuery and JQuery-UI


### Gredits
* Joona - Programming and UI design
* Joni - Icons and visualization
* Tia & Toni - Heatmaps
* Atte - visualization

![Map screenshot](/public/static/Images/map-screenshot.png "Map screenshot")